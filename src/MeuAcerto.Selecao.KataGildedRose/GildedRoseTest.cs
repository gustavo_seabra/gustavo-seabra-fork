﻿using System.Collections.Generic;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose
{
    public class GildedRoseTest
    {
        [Fact]
        public void ItensPassouValidade_DiminuiDuasVezes_Test()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Corselete +5 DEX", PrazoValidade = -1, Qualidade = 20 } };
            var valorEsperado = 18;
            
            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();

            Assert.Equal(valorEsperado, Items[0].Qualidade);
        }

        [Fact]
        public void ItensPassouValidade_QualidadeNegativa_Test()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Corselete +5 DEX", PrazoValidade = -1, Qualidade = 1 } };
            var valorEsperado = 0;

            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();

            Assert.Equal(valorEsperado, Items[0].Qualidade);
        }

        [Fact]
        public void Conjurados_DiminuiDuasVezes_Test()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Bolo de Mana Conjurado", PrazoValidade = 3, Qualidade = 6 } };
            var valorEsperado = 4;

            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();

            Assert.Equal(valorEsperado, Items[0].Qualidade);
        }

        [Fact]
        public void Conjurados_QualidadeNegativa_Test()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Bolo de Mana Conjurado", PrazoValidade = 3, Qualidade = 1 } };
            var valorEsperado = 0;

            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();

            Assert.Equal(valorEsperado, Items[0].Qualidade);
        }

        [Fact]
        public void QueijoBrie_AumentaQualidade_Test()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Queijo Brie Envelhecido", PrazoValidade = 8, Qualidade = 35 } };
            var valorEsperado = 36;

            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();

            Assert.Equal(valorEsperado, Items[0].Qualidade);
        }

        [Fact]
        public void Ingressos_AumentaQualidade_Test()
        {
            IList<Item> Items = new List<Item> { new Item
                {
                    Nome = "Ingressos para o concerto do TAFKAL80ETC",
                    PrazoValidade = 15,
                    Qualidade = 34
                } };
            var valorEsperado = 35;

            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();

            Assert.Equal(valorEsperado, Items[0].Qualidade);
        }

        [Fact]
        public void QueijoBrie_QualidadeMaior50_Test()
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Queijo Brie Envelhecido", PrazoValidade = 11, Qualidade = 52 } };
            var valorEsperado = 50;

            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();

            Assert.Equal(valorEsperado, Items[0].Qualidade);
        }

        [Fact]
        public void Ingressos_QualidadeMaior50_Test()
        {
            IList<Item> Items = new List<Item> { new Item
                {
                    Nome = "Ingressos para o concerto do TAFKAL80ETC",
                    PrazoValidade = 11,
                    Qualidade = 49
                } };
            var valorEsperado = 50;

            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();

            Assert.Equal(valorEsperado, Items[0].Qualidade);
        }

        [Fact]
        public void Ingressos_QualidadeAumenta2_Test()
        {
            IList<Item> Items = new List<Item> { new Item
                {
                    Nome = "Ingressos para o concerto do TAFKAL80ETC",
                    PrazoValidade = 8,
                    Qualidade = 22
                } };
            var valorEsperado = 24;

            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();

            Assert.Equal(valorEsperado, Items[0].Qualidade);
        }

        [Fact]
        public void Ingressos_QualidadeAumenta3_Test()
        {
            IList<Item> Items = new List<Item> { new Item
                {
                    Nome = "Ingressos para o concerto do TAFKAL80ETC",
                    PrazoValidade = 3,
                    Qualidade = 22
                } };
            var valorEsperado = 25;

            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();

            Assert.Equal(valorEsperado, Items[0].Qualidade);
        }

        [Fact]
        public void Ingressos_Qualidade0_Test()
        {
            IList<Item> Items = new List<Item> { new Item
                {
                    Nome = "Ingressos para o concerto do TAFKAL80ETC",
                    PrazoValidade = 0,
                    Qualidade = 22
                } };
            var valorEsperado = 0;

            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();

            Assert.Equal(valorEsperado, Items[0].Qualidade);
        }

        [Theory]
        [InlineData(4)]
        [InlineData(0)]
        [InlineData(-1)]
        public void Sulfuras_QualidadeImutavel_Test(int prazoValidade)
        {
            IList<Item> Items = new List<Item> { new Item { Nome = "Sulfuras, a Mão de Ragnaros", PrazoValidade = prazoValidade, Qualidade = 80 }, };
            var valorEsperado = 80;

            GildedRose app = new GildedRose(Items);
            app.AtualizarQualidade();

            Assert.Equal(valorEsperado, Items[0].Qualidade);
        }
    }
}
