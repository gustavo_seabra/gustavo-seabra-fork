﻿using System.Collections.Generic;
using System.Linq;

namespace MeuAcerto.Selecao.KataGildedRose
{
    class GildedRose
    {
        IList<Item> Itens;
        public GildedRose(IList<Item> Itens)
        {
            this.Itens = Itens;
        }

        public void AtualizarQualidade()
        {
            var lista = Itens.Where(i => i.Nome != "Sulfuras, a Mão de Ragnaros");
            ValidarItensPassouValidade(lista.Where(i => i.Nome != "Queijo Brie Envelhecido" &&  i.Nome != "Ingressos para o concerto do TAFKAL80ETC" && !i.Nome.Contains("Conjurado")).ToList());
            ValidarQueijoBrie(lista.Where(i => i.Nome.Contains("Queijo Brie")).ToList());
            ValidarIngresso(lista.Where(i => i.Nome.Contains("Ingressos")).ToList());
            ValidarItensConjurados(lista.Where(i => i.Nome.Contains("Conjurado")).ToList());
        }

        /// <summary>
        /// Quando o(PrazoValidade) do item tiver passado, a(Qualidade) do item diminui duas vezes mais rápido.
        /// </summary>
        /// <param name="itensPassadoValidade">Lista de itens vencidos</param>
        private void ValidarItensPassouValidade(IList<Item> itensPassadoValidade)
        {
            foreach (var item in itensPassadoValidade)
            {
                item.PrazoValidade -= 1;

                item.Qualidade = item.PrazoValidade < 0 ? item.Qualidade - 2 : item.Qualidade - 1;
                if (item.Qualidade < 0)
                    item.Qualidade = 0;
            }
        }

        /// <summary>
        /// O (Queijo Brie envelhecido), aumenta sua qualidade (Qualidade) ao invés de diminuir.
        /// </summary>
        /// <param name="itensPassadoValidade">Lista de itens vencidos</param>
        private void ValidarQueijoBrie(IList<Item> itensQueijoBrie)
        {
            foreach (var item in itensQueijoBrie)
            {
                item.PrazoValidade -= 1;

                item.Qualidade = item.PrazoValidade < 0 ? item.Qualidade + 2 : item.Qualidade + 1;
                if (item.Qualidade > 50)
                    item.Qualidade = 50;
            }
        }

        /// <summary>
        /// O item(Ingressos), assim como o(Queijo Brie envelhecido), aumenta sua(Qualidade) a medida que o(PrazoValidade) se aproxima;
        ///  A(Qualidade) aumenta em 2 unidades quando o(PrazoValidade) é igual ou menor que 10.
        ///  A(Qualidade) aumenta em 3 unidades quando o(PrazoValidade) é igual ou menor que 5.
        ///  A(Qualidade) do item vai direto à 0 quando o(PrazoValidade) tiver passado.
        /// </summary>
        /// <param name="itensIngresso"></param>
        private void ValidarIngresso(IList<Item> itensEIngresso)
        {
            foreach (var item in itensEIngresso)
            {
                item.PrazoValidade -= 1;

                if (item.PrazoValidade >= 5 && item.PrazoValidade < 10)
                    item.Qualidade += 2;
                else if (item.PrazoValidade >= 0 && item.PrazoValidade < 5)
                    item.Qualidade += 3;
                else if (item.PrazoValidade < 0)
                    item.Qualidade = 0;
                else
                    item.Qualidade += 1;

                if (item.Qualidade >= 50)
                    item.Qualidade = 50;
            }
        }

        /// <summary>
        /// Os itens "Conjurados" (Conjurado) diminuem a (Qualidade) duas vezes mais rápido
        /// </summary>
        /// <param name="itensConjurados">Lista de itens Conjurados</param>
        private void ValidarItensConjurados(IList<Item> itensConjurados)
        {
            foreach (var item in itensConjurados)
            {
                item.PrazoValidade -= 1;
                item.Qualidade = (item.Qualidade - 2) < 0 ? 0 : item.Qualidade - 2;
            }
        }
    }
}
